<?php

include_once('tg_conf.php');

function tg_bot($param){

  $url=BOT_API.$param;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $data = curl_exec($ch);
  curl_close($ch);
  return json_decode( $data, true );

}

function read_data($fn){
  $fp=fopen($fn,'r');
  $data=fread($fp,filesize($fp));
  fclose($fp);
  return $data;
}

function write_data($fn,$data,$mode){
  $fp=fopen($fn,$mode);
  fwrite($fp,$data);
  fclose($fp);
}

?>
